import PropTypes from "prop-types";

const ModalWrapper = ({children, click}) => {
    return (
        <div className="modal-wrapper" onClick={click}>
            {children}
        </div>
    );
};

export default ModalWrapper;

ModalWrapper.propTypes = {
    children: PropTypes.any,
    click: PropTypes.func,
}