import {createReducer} from "@reduxjs/toolkit";
import * as actions from './actions.js'

const initialState = {
    data: [],
    isOpenFirst: false,
}
export default createReducer(initialState, {
    [actions.setData]: (state, {payload}) => {
        state.data = payload
    },
    [actions.actionModal]: (state) => {
        state.isOpenFirst = !state.isOpenFirst
    }
})


