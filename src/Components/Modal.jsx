import PropTypes from "prop-types";
import Close from "./Close.jsx";
import './Components.scss'
import {useDispatch} from "react-redux";
import {actionModal} from "../store/actions.js";

const Modal = ({children}) => {

    const dispatch = useDispatch();

// відкриття і закриття модалки
    const actionFirst = () => {
        dispatch(actionModal());
    }

    const stopClick = (event) => {
        event.stopPropagation()
    }
    return (
        <>
            <div onClick={stopClick} className="modal">
                <Close click={actionFirst}/>
                {children}
            </div>
        </>
    );
};

export default Modal;

Modal.propTypes = {
    children: PropTypes.any
}