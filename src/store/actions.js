import {createAction} from "@reduxjs/toolkit";
import {sendRequest} from "../Helpers/SendRequest.jsx";

export const setData = createAction('SET_DATA')
export const actionModal = createAction('ACTION_MODAL')


export const fetchData = ()=>(dispatch)=>{
    return sendRequest('/data.json')
        .then((flowers)=>{
            dispatch(setData(flowers))
        })
}




