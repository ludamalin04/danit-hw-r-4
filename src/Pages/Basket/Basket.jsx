import PropTypes from "prop-types";
import BasketCard from "./BasketCard.jsx";
import Modal from "../../Components/Modal.jsx";
import ModalWrapper from "../../Components/ModalWrapper.jsx";
import ModalFooter from "../../Components/ModalFooter.jsx";
import Button from "../../Components/Button.jsx";
import {useDispatch, useSelector} from "react-redux";
import {actionModal} from "../../store/actions.js";

const Basket = ({
                    removeShop,
                    handleCurrentCard,
                    currentCard,
                    shop,
                    buyBtn,
                    buyClick,
                }) => {

    const dispatch = useDispatch();

// відкриття і закриття модалки
    const isOpenFirst = useSelector((state) => state.isOpenFirst)
    const actionFirst = () => {
        dispatch(actionModal());
    }

    return (
        <>
            {isOpenFirst && (
                <ModalWrapper click={actionFirst}>
                    <Modal><h2 className='basket-question'>Видалити товар?</h2>
                        <ModalFooter>
                            <Button click={actionFirst} classNames="modal-btn">СКАСУВАТИ</Button>
                            <Button click={() => {
                                actionFirst()
                                removeShop(currentCard.article)
                                buyClick(currentCard.article)
                            }}
                                    classNames="modal-btn-active"
                            >
                                ВИДАЛИТИ</Button>
                        </ModalFooter>
                    </Modal>
                </ModalWrapper>
            )}
            <div className='shop'>
                {
                    shop.map(({article, name, img, price, color}) => {
                        return (
                            <BasketCard
                                handleCurrentCard={handleCurrentCard}
                                buyBtn={buyBtn}
                                key={article}
                                alt={name}
                                img={img}
                                name={name}
                                article={article}
                                price={price}
                                color={color}
                            >
                            </BasketCard>
                        )
                    })
                }
                {shop.length === 0 && (
                    <h1>Ваш кошик порожній</h1>
                )}
            </div>
        </>
    );
};

Basket.propTypes = {
    shop: PropTypes.array,
    removeShop: PropTypes.func,
    handleCurrentCard: PropTypes.func,
    currentCard: PropTypes.object,
    buyBtn: PropTypes.any,
    buyClick: PropTypes.func,
}

export default Basket;